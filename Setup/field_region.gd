extends VFlowContainer

signal charachter_assigned(charachter_slot:CharachterSlot)

func _can_drop_data(_at_position, _data: Variant):
	return true


func _drop_data(_at_position, data: Variant):
	var dragged_data = data as CharachterSlot
	charachter_assigned.emit(dragged_data)
	var charachter = TextureRect.new()
	charachter.texture = dragged_data.texture
	charachter.ignore_texture_size = true
	charachter.custom_minimum_size = Vector2(96,96)
	charachter.size_flags_vertical = SIZE_SHRINK_CENTER ^ SIZE_EXPAND
	charachter.size_flags_horizontal = SIZE_SHRINK_CENTER
	add_child(charachter)