extends MarginContainer
class_name CharachterSlot
@icon("res://assets/icons/045-badge.png")

@onready var CharachterTexture = $TextureRect as TextureRect

@export var texture: Texture2D

@export var charachter: String = ""

var assignment: Setup.Assignment

func _ready():
	CharachterTexture.texture = texture

	
func _get_drag_data(at_position):
	var drag_texture = TextureRect.new()
	drag_texture.texture = CharachterTexture.texture
	drag_texture.size = size
	drag_texture.scale = Vector2(0.5,0.5)
	drag_texture.ignore_texture_size = true
	set_drag_preview(drag_texture)
	return self
	


func _on_margin_container_mouse_entered():
	scale = Vector2(1.2,1.2)


func _on_margin_container_mouse_exited():
	scale = Vector2(1,1)
