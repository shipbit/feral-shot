extends TextureRect
class_name DragContainer
@icon("res://assets/icons/014-gloves.png")
@export var charachter: String


func _get_drag_data(at_position):
	var data = {}
	data.charachter = charachter
	var drag_texture = TextureRect.new()
	drag_texture.texture = texture
	drag_texture.size = size
	drag_texture.ignore_texture_size = true
	drag_texture.custom_minimum_size = Vector2(60,60)
	set_drag_preview(drag_texture)
	return data


func _can_drop_data(at_position, data):
	return true

func _drop_data(at_position, data):
	pass;
