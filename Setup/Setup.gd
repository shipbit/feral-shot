extends Node2D
class_name Setup
@icon("res://assets/icons/037-team.png")

var player_count_attack = 0
var player_count_neutral = 0
var player_count_defense = 0

enum Assignment {
	Attack,
	Neutral,
	Defense
}

func _on_verteidigung_charachter_assigned(charachter_slot:CharachterSlot):
	player_count_defense+=1
	charachter_slot.assignment = Assignment.Defense
	check_ready()

func _on_mittelfeld_charachter_assigned(charachter_slot:CharachterSlot):
	player_count_neutral+=1
	charachter_slot.assignment = Assignment.Neutral
	check_ready()

func _on_angriff_charachter_assigned(charachter_slot:CharachterSlot):
	player_count_attack+=1
	charachter_slot.assignment = Assignment.Attack
	check_ready()

func check_ready():
	var player_count_reached = player_count_attack + player_count_neutral + player_count_defense == 7
	var all_fields = player_count_attack and player_count_neutral and player_count_defense

	$ReadyButton.visible = player_count_reached and all_fields

	