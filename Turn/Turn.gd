extends Node2D
class_name Turn
@icon("res://assets/icons/018-scoreboard.png")

@onready var Loop: TurnManager = $TurnManager
@onready var Battlers: Node = $Battlers


func _ready():
	for child in Battlers.get_children():
		var battler = child as Battler
		battler.ability_activated.connect(ability_activated)
		battler.battler_selected.connect(battler_selected)
		Loop.register_receiver(battler.player)

	Loop.start()

func ability_activated(player_identity, ability):
	if not Loop.has_turn(player_identity):
		printerr("Not your turn %s" %player_identity)
		return

	execute_ability(player_identity, ability)


func execute_ability(player_identity, ability):
	Loop.end_turn_and_proceed(player_identity)


func battler_selected(selected_battler: Battler):
	for child in Battlers.get_children():
		var battler = child as Battler
		if battler != selected_battler:
			battler.set_selected(false)

func _on_turn_manager_turns_exhausted(identity):
	pass


func _on_turn_manager_turn_started(identity):
	for child in Battlers.get_children():
		var battler = child as Battler
		if battler.player == identity:
			battler.set_turn(true)


func _on_turn_manager_turn_ended(identity):
	for child in Battlers.get_children():
		var battler = child as Battler
		if battler.player == identity:
			battler.set_turn(false)

