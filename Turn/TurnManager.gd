class_name TurnManager
extends Node

signal turn_started(identity)
signal turn_ended(identity)
signal turns_exhausted(identity)

var turn_receivers: Array = []
var current_turn: int = 0

func start():
	current_turn = 0
	turn_started.emit(turn_receivers[current_turn])

func end_turn_and_proceed(identity):
	var is_identity_in_play = turn_receivers.has(identity)
	var has_identity_current_turn = turn_receivers[current_turn] == identity

	if not is_identity_in_play or not has_identity_current_turn:
		printerr("invalid turn request %s" %identity)
		return

	if turn_receivers.size() < 2:
		printerr("turns exhausted %s" %turn_receivers[0])
		return

	turn_ended.emit(identity)
	
	current_turn = (current_turn + 1 + turn_receivers.size()) % turn_receivers.size()

	turn_started.emit(turn_receivers[current_turn])

func register_receiver(identity):
	if not turn_receivers.has(identity):
		turn_receivers.append(identity)

func has_turn(identity):
	return turn_receivers[current_turn] == identity