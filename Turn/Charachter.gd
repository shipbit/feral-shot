class_name Battler
extends TextureButton

var has_turn: bool = true
var selected: bool = true
@export var player: String = "Player 1"
@export var ability1: String = "Block"
@export var ability2: String = "Foul"
@export var ability1Icon: Texture2D
@export var ability2Icon: Texture2D


signal ability_activated(player_identity, ability_identity)
signal battler_selected(battler)

func _ready():
	$Abilities/Ability1.text = ability1
	$Abilities/Ability2.text = ability2
	$Abilities/Ability1.icon = ability1Icon
	$Abilities/Ability2.icon = ability2Icon
	set_turn(false)
	set_selected(false)

func _on_battler_pressed():
	if not has_turn:
		return
	
	set_selected(true)

func _on_ability_2_pressed():
	if not has_turn:
		return

	ability_activated.emit(player,ability2)

func _on_ability_1_pressed():
	if not has_turn:
		return

	ability_activated.emit(player,ability1)


func set_turn(value: bool):
	if value == has_turn:
		return

	has_turn = value
	if not has_turn:
		set_selected(false)
		modulate = Color.DARK_GRAY
	else:
		modulate = Color.WHITE
	
func set_selected(value: bool):
	if value == selected:
		return

	selected = value
	$Abilities.visible = value and has_turn
	
	if selected:
		scale = Vector2(1,1)
		battler_selected.emit(self)
	else:
		scale = Vector2(0.8,0.8)
